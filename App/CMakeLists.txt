project(RabbitCommonApp)

set(SOURCE_FILES
    main.cpp
    )

#翻译
include(${CMAKE_SOURCE_DIR}/cmake/Qt5CorePatches.cmake)
include(${CMAKE_SOURCE_DIR}/cmake/Translations.cmake)

set(RESOURCE_FILES Resource/Resource.qrc
    "${TRANSLATIONS_RESOURCE_FILES}")

if(WIN32)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
endif()
IF(MSVC)
    add_compile_options("$<$<C_COMPILER_ID:MSVC>:/utf-8>")
    add_compile_options("$<$<CXX_COMPILER_ID:MSVC>:/utf-8>")
ENDIF()
add_executable(${PROJECT_NAME} ${SOURCE_FILES})
#add_dependencies(${PROJECT_NAME} translations_${TRANSLATIONS_NAME})
if(BUILD_ABOUT)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DHAVE_ABOUT)
endif()
if(BUILD_UPDATE)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DHAVE_UPDATE)
endif()
if(MINGW)
    set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS_RELEASE "-mwindows")
elseif(MSVC)
    set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS_RELEASE
        "/SUBSYSTEM:WINDOWS\",5.01\" /ENTRY:mainCRTStartup")
endif()
target_link_libraries(${PROJECT_NAME} PRIVATE RabbitCommon ${QT_LIBRARIES})
target_include_directories(${PROJECT_NAME} PRIVATE
      "${CMAKE_BINARY_DIR}/Src" "${CMAKE_SOURCE_DIR}/Src" )
# Install target
INSTALL(TARGETS ${PROJECT_NAME}
        RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}"
        ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        )
    
# Install other files
INSTALL(FILES ${OTHER_FILES} DESTINATION ".")

IF("Release" STREQUAL CMAKE_BUILD_TYPE AND WIN32)
    SET(RUNTIME_FILE "${CMAKE_BINARY_DIR}/bin/${PROJECT_NAME}${CMAKE_EXECUTABLE_SUFFIX}")
    IF(CMAKE_BUILD_TYPE MATCHES Release AND MINGW)
        #windeployqt 分发时，是根据是否 strip 来判断是否是 DEBUG 版本,而用mingw编译时,qt没有自动 strip
        add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
            COMMAND strip "$<TARGET_FILE:${PROJECT_NAME}>"
            )
    ENDIF()

    #注意 需要把 ${QT_INSTALL_DIR}/bin 加到环境变量PATH中  
    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND "${QT_INSTALL_DIR}/bin/windeployqt"
        --compiler-runtime
        --verbose 7
        "$<TARGET_FILE:${PROJECT_NAME}>"
        )
    INSTALL(DIRECTORY "$<TARGET_FILE_DIR:${PROJECT_NAME}>/"
        DESTINATION "${CMAKE_INSTALL_BINDIR}")
    
ENDIF()

#IF(ANDROID)
#    GENERATED_DEPLOYMENT_SETTINGS()
#    find_program(ANT NAMES ant) # PATHS  "/usr/bin")
#    MESSAGE("ant:${ANT}\n")

#    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
#        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/libRabbitIm.so ${CMAKE_BINARY_DIR}/android-build/libs/${ANDROID_ABI}/libRabbitIm.so
#        #注意 需要把 ${QT_INSTALL_DIR}/bin 加到环境变量PATH中
#        COMMAND "${QT_INSTALL_DIR}/bin/androiddeployqt"
#            --input ${CMAKE_BINARY_DIR}/android-libRabbitIm.so-deployment-settings.json
#            --output ${CMAKE_BINARY_DIR}/android-build
#            --verbose
#            #--ant /usr/bin/ant
#            #--jdk $ENV{JAVA_HOME}
#    )
#ENDIF(ANDROID)
